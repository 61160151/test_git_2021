﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace DemoSope
{
    /// <summary>
    /// Summary description for WebService1
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WebService1 : System.Web.Services.WebService
    {
        DataBaseHelper db = new DataBaseHelper();
        [WebMethod]
        public string HelloWorld(String name)
        {
           
            return "Hello World : " + name;
        }
        [WebMethod]
        public DataTable GetNationality()
        { 

            DataTable dt = db.ExecuteDataTable("sel_nationality");
            dt.TableName = "NameofTheTable";
            return dt;
        }
        public void SetNationality()
        {

        }
    }
}
