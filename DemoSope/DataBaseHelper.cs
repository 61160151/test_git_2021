﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace DemoSope
{
    class DataBaseHelper
    {
        private DataSet ds = new DataSet();
        private DataTable dt = new DataTable();
        private string cnnString = "";
        private CommandType commandType = CommandType.StoredProcedure;

        public string ConnectionString
        {
            get
            {
                if (cnnString == "")
                    cnnString = ConfigurationManager.ConnectionStrings["DBCon"].ConnectionString;
                return cnnString;
            }
            set
            {
                cnnString = value;
            }
        }
        public CommandType SQLCommandType
        {
            get
            {
                return commandType;
            }
            set
            {
                commandType = value;
            }
        }


        public void ExecuteNonQuery(string ProcName, SqlParameter[] ParameterArray, out SqlParameterCollection ParamCollection)
        {
            string connStr = ConfigurationManager.ConnectionStrings["DBCon"].ConnectionString;

            using (SqlConnection Conn = new SqlConnection(connStr))
            {
                Conn.Open();
                SqlCommand Cmd = new SqlCommand(ProcName, Conn);
                Cmd.CommandType = CommandType.StoredProcedure;

                for (int i = 0; i < ParameterArray.Length; i++)
                    Cmd.Parameters.Add(ParameterArray[i]);

                Cmd.ExecuteNonQuery();

                ParamCollection = Cmd.Parameters;

                Conn.Close();
            }
        }

        public void ExecuteSQL(string SQL)
        {
            String conSTR = ConfigurationManager.ConnectionStrings["DBCon"].ConnectionString;
            SqlConnection con = new SqlConnection(conSTR);
            con.Open();

            SqlCommand cmd = new SqlCommand(SQL);
            SqlDataAdapter da = new SqlDataAdapter(cmd.CommandText, con);

            da.Fill(dt);
        }



        public int ExecuteNonQuery(string sql)
        {
            return this.ExecuteNonQuery(sql, new List<SqlParameter>());
        }

        public int ExecuteNonQuery(string sql, List<SqlParameter> param)
        {
            SqlConnection cnn = new SqlConnection(this.ConnectionString);
            SqlCommand cmm = new SqlCommand(sql, cnn);
            cmm.CommandType = this.SQLCommandType;

            int retValue = 0;

            for (int i = 0; i < param.Count; i++)
            {
                cmm.Parameters.Add(param[i]);
            }
            try
            {
                if (cnn.State == ConnectionState.Closed) cnn.Open();

                retValue = cmm.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                cnn.Close();
            }

            return retValue;
        }

        public object ExecuteScalar(string sql)
        {
            return this.ExecuteScalar(sql, new List<SqlParameter>());
        }

        public object ExecuteScalar(string sql, List<SqlParameter> param)
        {
            SqlConnection cnn = new SqlConnection(this.ConnectionString);
            SqlCommand cmm = new SqlCommand(sql, cnn);
            if (sql.IndexOf("SELECT") > -1 && sql.IndexOf("FROM") > -1)
                cmm.CommandType = CommandType.Text;
            else
                cmm.CommandType = this.SQLCommandType;
            object retValue = 0;

            for (int i = 0; i < param.Count; i++)
            {
                cmm.Parameters.Add(param[i]);
            }
            try
            {
                if (cnn.State == ConnectionState.Closed) cnn.Open();

                retValue = cmm.ExecuteScalar();
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                cnn.Close();
            }

            return retValue;
        }

        public DataTable ExecuteDataTable(string sql)
        {
            return this.ExecuteDataTable(sql, new List<SqlParameter>());
        }

        public DataTable ExecuteDataTable(string sql, List<SqlParameter> param)
        {
            SqlConnection cnn = new SqlConnection(this.ConnectionString);
            SqlDataAdapter ad = new SqlDataAdapter(sql, cnn);
            DataTable dt = new DataTable();
            ad.SelectCommand.CommandType = this.SQLCommandType;

            for (int i = 0; i < param.Count; i++)
            {
                ad.SelectCommand.Parameters.Add(param[i]);
            }
            try
            {
                if (cnn.State == ConnectionState.Closed) cnn.Open();
                ad.Fill(dt);
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                cnn.Close();
            }

            return dt;
        }

        public DataSet ExecuteDataSet(string sql)
        {
            return this.ExecuteDataSet(sql, new List<SqlParameter>());
        }

        public DataSet ExecuteDataSet(string sql, List<SqlParameter> param)
        {
            SqlConnection cnn = new SqlConnection(this.ConnectionString);
            SqlDataAdapter ad = new SqlDataAdapter(sql, cnn);
            DataSet ds = new DataSet();
            ad.SelectCommand.CommandType = this.SQLCommandType;

            for (int i = 0; i < param.Count; i++)
            {
                ad.SelectCommand.Parameters.Add(param[i]);
            }
            try
            {
                if (cnn.State == ConnectionState.Closed) cnn.Open();

                ad.Fill(ds);
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                cnn.Close();
            }

            return ds;
        }
    }
}